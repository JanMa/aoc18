# Advent of code 2018

This repository contains my solutions for [Advent of code 2018](https://adventofcode.com/2018). I'll try to solve the puzzles with bash scripts first and if i have time, also in another programming language (probably Go or C).