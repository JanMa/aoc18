#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    FILE *input = fopen("input.txt", "r");    
    int freq = 0;
    int len = 10;
    char line[len];
    while(fgets(line, len, input) != NULL){
        freq += atoi(line);
    }
    printf("%d\n", freq);
    fclose(input);
    return 0;
}

