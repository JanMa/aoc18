package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	input, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()

	freq := 0
	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		i, _ := strconv.Atoi(scanner.Text())
		freq += i
	}

	fmt.Println(freq)
}
