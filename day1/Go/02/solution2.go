package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

var (
	freq  = 0
	loop  = 1
	m     = make(map[int]int)
	freqs []int
)

func main() {
	input, err := os.Open(os.Args[1])
	scanner := bufio.NewScanner(input)
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()

	//parse input in array
	for scanner.Scan() {
		i, _ := strconv.Atoi(scanner.Text())
		freqs = append(freqs, i)
	}

	// loop until value is found
	for loop == 1 {
		for _, i := range freqs {
			freq += i
			if _, ok := m[freq]; ok {
				fmt.Println(freq)
				loop = 0
				break
			}
			m[freq] = 1
		}
	}
}
