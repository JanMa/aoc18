#!/bin/bash
while IFS= read line; do
    (( FREQ += line ))
done <<< "$(cat "$1")"
echo "$FREQ"