#!/bin/bash
declare -A FREQS=( [0]=1 )
while true; do
    while IFS= read line; do
        (( FREQ += line ))
        [[ -n "${FREQS[$FREQ]}" ]] && echo "$FREQ" && exit 0
        FREQS[$FREQ]=1
    done <<< "$(cat "$1")"
done