package main

import (
	"fmt"
	"gitlab.com/JanMa/aoc18/utils"
	"math"
	"strconv"
)

func getHundred(val int) int {
	str := strconv.Itoa(val)
	ret := 0
	if len(str) > 2 {
		ret, _ = strconv.Atoi(string(str[len(str)-3]))
	}
	return ret
}

func main() {
	input := utils.ScanFile()
	serial, _ := strconv.Atoi(input[0])
	var field [301][301]int
	for j := 1; j <= 300; j++ {
		for i := 1; i <= 300; i++ {
			field[i][j] = getHundred(((i+10)*j+serial)*(i+10)) - 5
			//fmt.Println(field[j][i])
		}
	}
	maxPower := 0
	var maxCoord = utils.Coord{0, 0}
	for j := 1; j <= 300; j++ {
		for i := 1; i <= 300; i++ {
			tmpPow := getPower(utils.Coord{i, j}, field)
			if tmpPow > maxPower {
				maxPower = tmpPow
				maxCoord = utils.Coord{i, j}
			}
		}
	}
	fmt.Println("Part 1:")
	fmt.Printf("%d at %d,%d\n", maxPower, maxCoord.X, maxCoord.Y)
	maxPower = 0
	maxCoord = utils.Coord{0, 0}
	maxSize := 0
	for j := 1; j <= 300; j++ {
		for i := 1; i <= 300; i++ {
			tmpPow, tmpSize := getMaxPower(utils.Coord{i, j}, field)
			if tmpPow > maxPower {
				maxPower = tmpPow
				maxCoord = utils.Coord{i, j}
				maxSize = tmpSize
			}
		}
	}
	fmt.Println("Part 2:")
	fmt.Printf("%d at %d,%d,%d\n", maxPower, maxCoord.X, maxCoord.Y, maxSize)
}

func getPower(c utils.Coord, f [301][301]int) int {
	power := 0
	if c.X > 0 && c.Y > 0 && c.X < 299 && c.Y < 299 {
		for i := 0; i < 3; i++ {
			for j := 0; j < 3; j++ {
				power += f[c.X+i][c.Y+j]
			}
		}
	}
	return power
}
func getMaxPower(c utils.Coord, f [301][301]int) (int, int) {
	maxPower := 0
	maxSize := 0
	if c.X > 0 && c.Y > 0 && c.X < 299 && c.Y < 299 {
		k := int(math.Max(float64(c.X), float64(c.Y)))
		for s := 1; s < 299-k; s++ {
			power := 0
			for i := 0; i < s; i++ {
				for j := 0; j < s; j++ {
					power += f[c.X+i][c.Y+j]
				}
			}
			if power > maxPower {
				maxPower = power
				maxSize = s
			}
		}
	}
	return maxPower, maxSize
}
