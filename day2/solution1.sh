#!/bin/bash
TWO=0
THREE=0

while IFS= read line; do
    DUP="$(echo "$line" | grep -o . | sort | uniq -dc)"
    echo "$DUP" | grep -qE "2 "
    if [[ ${?} -eq 0 ]]; then
        (( TWO += 1 ))
    fi
    echo "$DUP" | grep -qE "3 "
    if [[ ${?} -eq 0 ]]; then
        (( THREE += 1 ))
    fi
done <<< "$(cat $1)"
echo "$(( TWO * THREE ))"
