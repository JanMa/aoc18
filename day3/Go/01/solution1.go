package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

var ()

func main() {
	var fabric [1000][1000]int
	sum := 0
	regex := *regexp.MustCompile(`.[0-9]+.@.([0-9]+),([0-9]+):.([0-9]+)x([0-9]+)`)
	input, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		res := regex.FindAllStringSubmatch(scanner.Text(), -1)
		//fmt.Printf("%s %s %s %s\n", res[0][1], res[0][2], res[0][3], res[0][4])
		x, _ := strconv.Atoi(res[0][1])
		y, _ := strconv.Atoi(res[0][2])
		sx, _ := strconv.Atoi(res[0][3])
		sy, _ := strconv.Atoi(res[0][4])
		for i := 0; i < sx; i++ {
			for j := 0; j < sy; j++ {
				fabric[x+i][y+j]++
			}
		}
	}
	for i := 0; i < 1000; i++ {
		for j := 0; j < 1000; j++ {
			if fabric[i][j] > 1 {
				sum++
			}
		}
	}
	fmt.Println(sum)
}
