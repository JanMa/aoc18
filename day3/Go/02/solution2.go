package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type claim struct {
	id   int
	x, y int
	w, h int
}

func main() {
	var fabric [1000][1000]int
	var claims []claim
	input, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		var c claim
		fmt.Sscanf(scanner.Text(), "#%d @ %d,%d: %dx%d", &c.id, &c.x, &c.y, &c.w, &c.h)
		claims = append(claims, c)
	}

	for cla := range claims {
		for i := 0; i < claims[cla].w; i++ {
			for j := 0; j < claims[cla].h; j++ {
				fabric[claims[cla].x+i][claims[cla].y+j]++
			}
		}
	}
	for cla := range claims {
		sum := 0
		for i := 0; i < claims[cla].w; i++ {
			for j := 0; j < claims[cla].h; j++ {
				sum += fabric[claims[cla].x+i][claims[cla].y+j]
			}
		}
		if sum == claims[cla].w*claims[cla].h {
			fmt.Println(claims[cla].id)
		}
	}
}
