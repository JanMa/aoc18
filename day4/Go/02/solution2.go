package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

type entry struct {
	date   time.Time
	action action
	guard  int
}

type action int

const (
	beginShift action = iota
	fallAsleep
	wakeUp
)

func main() {
	entries := read()

	var sleepyguard, sleepyminute int
	minutes := map[int]*[60]int{}
	var guard, from int
	for _, e := range entries {
		switch e.action {
		case beginShift:
			guard = e.guard
			if minutes[guard] == nil {
				minutes[guard] = &[60]int{}
			}
			if minutes[sleepyguard] == nil {
				sleepyguard = guard
			}

		case fallAsleep:
			from = e.date.Minute()
		case wakeUp:
			to := e.date.Minute()
			for i := from; i < to; i++ {
				minutes[guard][i]++
				if minutes[guard][i] > minutes[sleepyguard][sleepyminute] {
					sleepyguard = guard
					sleepyminute = i
				}
			}
		}
	}

	fmt.Printf("Guard %d * minute %d = %d\n",
		sleepyguard, sleepyminute, sleepyguard*sleepyminute)
}

func read() []entry {
	entries := []entry{}
	//bad but easy hack :-D
	cmd := exec.Command("sort", os.Args[1])
	sorted, err := cmd.StdoutPipe()
	scanner := bufio.NewScanner(sorted)
	if err != nil {
		log.Fatal(err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	for scanner.Scan() {
		txt := scanner.Text()
		e := entry{guard: -1}

		var year, month, day, hour, min int
		n, err := fmt.Sscanf(scanner.Text(), "[%d-%d-%d %d:%d]", &year, &month, &day, &hour, &min)
		if n < 5 || err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
			continue
		}
		e.date = time.Date(year, time.Month(month), day, hour, min, 0, 0, time.UTC)

		i := strings.Index(txt, "] ")
		if i == -1 {
			fmt.Fprintf(os.Stderr, "ERROR: unable to parse message\n")
			continue
		}
		txt = txt[i+2:]
		n, err = fmt.Sscanf(txt, "Guard #%d begins shift", &e.guard)
		switch {
		case n == 1:
			e.action = beginShift
		case txt == "falls asleep":
			e.action = fallAsleep
		case txt == "wakes up":
			e.action = wakeUp
		default:
			fmt.Fprintf(os.Stderr, "ERROR: unknown action\n")
			continue
		}
		entries = append(entries, e)
	}
	return entries
}
