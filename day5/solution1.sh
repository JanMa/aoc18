#!/bin/bash
input=$(cat $1)

len=0

while [[ $len -ne ${#input} ]]; do
    len=${#input}
    for i in {a..z}; do
        input=${input//$i${i^^}}
        input=${input//${i^^}$i}
    done
done
echo "${#input}"
