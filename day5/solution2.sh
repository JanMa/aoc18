#!/bin/bash
input=$(cat $1)
min=${#input}
for j in {a..z}; do
    len=0
    tmp=$input
    tmp=${tmp//$j}
    tmp=${tmp//${j^^}}
    while [[ $len -ne ${#tmp} ]]; do
        len=${#tmp}
        for i in {a..z}; do
            tmp=${tmp//$i${i^^}}
            tmp=${tmp//${i^^}$i}
        done
    done
    echo "Remove $j/${j^^}: ${#tmp}"
    if [[ ${#tmp} -lt $min ]]; then
        min=${#tmp}
    fi
done
echo "Minimum: $min"