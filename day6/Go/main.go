package main

import (
	"fmt"
	"gitlab.com/JanMa/aoc18/utils"
	"math"
)

type coord struct {
	x, y int
	inf  bool
}

func distance(x int, y int, c coord) int {
	return int(math.Abs(float64(x)-float64(c.x)) + math.Abs(float64(y)-float64(c.y)))
}

func main() {
	input := utils.ScanFile()
	coords := make(map[int]coord)
	count := make(map[int]int)
	var minX, minY, maxX, maxY, id int
	minX = 1000
	minY = 1000
	id = 1000
	for _, line := range input {
		var tmpX, tmpY int
		fmt.Sscanf(line, "%d, %d", &tmpX, &tmpY)
		if tmpX < minX {
			minX = tmpX
		}
		if tmpX > maxX {
			maxX = tmpX
		}
		if tmpY < minY {
			minY = tmpY
		}
		if tmpY > maxY {
			maxY = tmpY
		}
		coords[id] = coord{tmpX, tmpY, false}
		id++
	}
	var fabric [500][500]int
	finalPatch := 0
	for i := 0; i < minX+maxX; i++ {
		for j := 0; j < minY+maxY; j++ {
			tmpMin := 1000
			var tmpID int
			totalDist := 0
			for k, c := range coords {
				dst := distance(i, j, c)
				totalDist += dst
				if dst < tmpMin {
					tmpMin = dst
					tmpID = k
				}
			}
			for k, c := range coords {
				dst := distance(i, j, c)
				if dst == tmpMin && k != tmpID {
					tmpID = -1
				}
			}
			fabric[i][j] = tmpID
			if (i == minX || i == maxX || j == minY || j == maxY) && tmpID != -1 {
				coords[tmpID] = coord{coords[tmpID].x, coords[tmpID].y, true}
			}
			count[tmpID]++
			if totalDist < 10000 {
				finalPatch++
			}
		}
	}
	var max, maxID int
	for k := range coords {
		if coords[k].inf == false && count[k] > max {
			maxID = k
			max = count[k]
		}
	}
	fmt.Println("Part1:")
	fmt.Printf("MaxID: %d with %d\n", maxID, max)
	fmt.Println("Part2:")
	fmt.Printf("Final patch size: %d\n", finalPatch)
}
