#!/bin/bash
regex='([0-9]+), ([0-9]+)'
ID=1000
max=$(grep -oE "[0-9]+" $1 | sort -n | tail -n1)
min=$(grep -oE "[0-9]+" $1 | sort -n | head -n1)
echo "$min,$max"
declare -A map
while IFS= read line; do
    # echo "$line"
    if [[ $line =~ $regex ]]; then
        x=${BASH_REMATCH[1]}
        y=${BASH_REMATCH[2]}
        map[$x,$y]=$ID
        (( ID += 1 ))
    fi
done < $1
declare -a coords=( ${map[@]} )
function dist {
    local x=$(( $1 - $3 ))
    local y=$(( $2 - $3 ))
    local dist=$(( ${1#-} + ${2#-} ))
    echo "$dist"
}
for (( i=$min; i<=$max; i++ )); do
    for (( j=$min; j<=$max; j++ )); do
        if [[ -z map[$i,$j] ]]; then
            local mindist=0
            for c in ${coords[@]}; do

