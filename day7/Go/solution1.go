package main

import (
	"fmt"
	"gitlab.com/JanMa/aoc18/utils"
	"sort"
)

func part1() {
	input := utils.ScanFile()
	instructions, parents := generateInput(input)
	done := getFirst(instructions, parents)
	fmt.Println(getAnswer(done, instructions, parents))
}

func getFirst(i map[rune][]rune, p map[rune]int) []rune {
	var first = []rune{}
	for k := range i {
		if p[k] == 0 {
			first = append(first, k)
		}
	}
	return first
}
func generateInput(input []string) (map[rune][]rune, map[rune]int) {
	instructions := make(map[rune][]rune)
	parents := make(map[rune]int)

	for _, k := range input {
		key := rune(k[5])
		value := rune(k[36])
		instructions[key] = append(instructions[key], value)
		parents[value] = parents[value] + 1
	}
	return instructions, parents
}
func getAnswer(d []rune, i map[rune][]rune, p map[rune]int) string {
	answer := ""
	if len(d) > 0 {
		tmp := make([]rune, len(d))
		copy(tmp, d)
		sort.Sort(utils.Runes(tmp))
		first := tmp[0]
		for i := 0; i < len(d); i++ {
			if d[i] == first {
				d = append(d[:i], d[i+1:]...)
			}
		}
		answer += string(first)
		for _, v := range i[first] {
			p[v]--
			if p[v] == 0 {
				d = append(d, v)
			}
		}
		answer += getAnswer(d, i, p)
	}
	return answer
}
