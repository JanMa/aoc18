package main

import (
	"fmt"
	"gitlab.com/JanMa/aoc18/utils"
	"sort"
)

func part2() {
	input := utils.ScanFile()
	instructions, parents := generateInput(input)
	ready := getFirst(instructions, parents)
	finished := []rune{}
	workerTasks := []rune{'.', '.', '.', '.', '.'}
	timeLeft := []int{0, 0, 0, 0, 0}
	time := 0
	atWork := 1
	for ; atWork > 0; time++ {
		atWork = 0
		for n := range timeLeft {
			if timeLeft[n] != 0 {
				timeLeft[n]--
				atWork++
			} else {
				if workerTasks[n] != '.' {
					fin := workerTasks[n]
					workerTasks[n] = '.'
					for _, v := range instructions[fin] {
						parents[v]--
						if parents[v] == 0 {
							ready = append(ready, v)
						}
					}
				}
			}
		}
		for len(ready) > 0 && atWork < len(timeLeft) {
			tmp := make([]rune, len(ready))
			copy(tmp, ready)
			sort.Sort(utils.Runes(tmp))
			first := tmp[0]
			for i := 0; i < len(ready); i++ {
				if ready[i] == first {
					ready = append(ready[:i], ready[i+1:]...)
				}
			}
			finished = append(finished, first)
			for n := range timeLeft {
				if workerTasks[n] == '.' {
					workerTasks[n] = first
					timeLeft[n] = int(first) - 5
					atWork++
					break
				}
			}
		}
		fmt.Print(time, " ")
		for n := range timeLeft {
			fmt.Print("{", timeLeft[n], " ", string(workerTasks[n]), "}")
		}
		fmt.Print(string(finished), "\n")
	}
	fmt.Println(time - 1)
}
