package main

import (
	"fmt"
	"gitlab.com/JanMa/aoc18/utils"
	"regexp"
	"strconv"
)

type node struct {
	c, m   int
	meta   []int
	childs []int
}

func main() {
	re := regexp.MustCompile(`[0-9]+`)
	input := utils.ScanFile()
	found := re.FindAllStringSubmatch(input[0], -1)
	list := []int{}
	for i := range found {
		num, _ := strconv.Atoi(string(found[i][0]))
		list = append(list, num)
	}
	_, nodes := getNodes(0, list, []node{})
	sum := 0
	for i := range nodes {
		for _, j := range nodes[i].meta {
			sum += j
		}
	}
	fmt.Println("Part1:")
	fmt.Printf("Sum: %d\n", sum)
	fmt.Println("Part2:")
	fmt.Println("Root sum:", getSum(len(nodes)-1, nodes))
}

func getSum(node int, nodes []node) int {
	n := nodes[node]
	sum := 0
	if n.c == 0 {
		for _, i := range n.meta {
			sum += i
		}
	} else {
		for i := 0; i < n.m; i++ {
			nextNode := n.meta[i] - 1
			if nextNode < n.c {
				sum += getSum(n.childs[nextNode], nodes)
			}
		}
	}
	return sum
}

func getNodes(offset int, list []int, nodes []node) (int, []node) {
	off := offset
	if off < len(list) {
		children := list[off]
		meta := list[off+1]
		off += 2
		metaEntries := []int{}
		childs := []int{}
		if children == 0 {
			metaEntries = list[off : off+meta]
			off += meta
			nodes = append(nodes, node{c: children, m: meta, meta: metaEntries})
		} else if children > 0 {
			for i := 0; i < children; i++ {
				newOff, newNodes := getNodes(off, list, nodes)
				off = newOff
				nodes = newNodes
				childs = append(childs, len(nodes)-1)
			}
			metaEntries = list[off : off+meta]
			off += meta
			nodes = append(nodes, node{c: children, m: meta, meta: metaEntries, childs: childs})
		}
	}
	return off, nodes
}
