package main

import (
	"container/ring"
	"fmt"
	"gitlab.com/JanMa/aoc18/utils"
)

func main() {
	input := utils.ScanFile()
	var players, marbles int
	fmt.Sscanf(input[0], "%d players; last marble is worth %d", &players, &marbles)
	fmt.Println(players, marbles)
	scores1 := Marbles(players, marbles)
	h1, _ := utils.FindMax(scores1)
	scores2 := Marbles(players, marbles*100)
	h2, _ := utils.FindMax(scores2)
	fmt.Println("Solution 1:")
	fmt.Println(h1)
	fmt.Println("Solution 2:")
	fmt.Println(h2)
}

//Marbles calculate scores
func Marbles(players, last int) []int {
	circle := ring.New(1)
	circle.Value = 0

	scores := make([]int, players)

	for i := 1; i <= last; i++ {
		if i%23 == 0 {
			circle = circle.Move(-8)
			removed := circle.Unlink(1)
			scores[i%players] += i + removed.Value.(int)
			circle = circle.Move(1)
		} else {
			circle = circle.Move(1)

			s := ring.New(1)
			s.Value = i

			circle.Link(s)
			circle = circle.Move(1)
		}
	}
	return scores
}
