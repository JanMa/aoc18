package utils

import (
	"bufio"
	"log"
	"os"
)

// Runes an array of rune
type Runes []rune

// Coord point in a coordinate system
type Coord struct {
	X, Y int
}

// ScanFile Returns all lines in a file passed as first argument
func ScanFile() []string {
	if len(os.Args) < 2 {
		log.Fatal("No input specified!")
	}
	var lines []string
	input, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

// Len returns lenght of array
func (r Runes) Len() int { return len(r) }

// Swap swaps two elements in array
func (r Runes) Swap(i, j int) { r[i], r[j] = r[j], r[i] }

// Less returns true if i < j
func (r Runes) Less(i, j int) bool { return r[i] < r[j] }

// FindMax returns max value and index of int array
func FindMax(vals []int) (int, int) {
	var max, elem int
	for k, v := range vals {
		if v > max {
			max = v
			elem = k
		}
	}
	return max, elem
}
